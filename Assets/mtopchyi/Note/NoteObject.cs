using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

public class NoteObject : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI title;
    [SerializeField] private TextMeshProUGUI description;
    [SerializeField] private TextMeshProUGUI date;
    
    public string Title => title.text;
    public string Description => description.text;

    [SerializeField] private Note note;

    [SerializeField] private NoteView noteView;
    
    public void Init(Note note, NoteView noteView)
    {
        this.note = note;
        title.text = note.Title;
        description.text = note.Description;
        date.text = note.Date;
        this.noteView = noteView;
    }
    
    public void Open()
    {
        noteView.OpenNote(note);
    }

}
