using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Threading.Tasks;
using DG.Tweening;
using Firebase.Storage;
using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using System.Web;
public class NoteView : MonoBehaviour
{
    public string Title => title.text;
    public string Description => description.text;

    public bool IsOpen = false;
    
    [SerializeField] private InputField title;
    [SerializeField] private InputField description;

    [SerializeField] private Note currentNote;
    
    private string ttitle;
    private string tdesc;
    

    public void Open()
    {
        transform.DOScale(Vector3.one, 0.5f);
        IsOpen = true;
        currentNote = new Note();
    }
    public void OnChanged()
    {
        if(!Input.GetKeyDown(KeyCode.Escape))
        {
            ttitle = Title;
            tdesc = Description;
        }
    }
    public void OpenNote(Note note)
    {
        currentNote = note;
        title.text = note.Title;
        
        description.text = note.Description;
        
        transform.DOScale(Vector3.one, 0.5f);
        IsOpen = true;
    }
    public void CloseNote()
    {
        IsOpen = false;
        transform.DOScale(Vector3.zero, 0.5f);
        
        if(currentNote.Title!=ttitle || currentNote.Description!= tdesc)
        {
            SaveNote();
        }
        
        NetworkController.Instance.EditNote(currentNote);
    }
    public void DeleteNote()
    {
        IsOpen = false;
        transform.DOScale(Vector3.zero, 0.5f);
        NetworkController.Instance.DeleteNote(currentNote);
    }
    public void Close()
    {
        IsOpen = false;
        transform.DOScale(Vector3.zero, 0.5f);

        SaveNote();

        NetworkController.Instance.AddNote(currentNote);
        
        ClearNote();
    }
    private void SaveNote()
    {
        currentNote.Title = ttitle; 
        currentNote.Description = tdesc;
        currentNote.Date = DateTime.Now.ToString();
    }
    private void ClearNote()
    {
        ttitle = "";
        ttitle = "";
        
        title.text = "";
        description.text = "";
    }
}



