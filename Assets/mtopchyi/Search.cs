using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Firebase.Database;
using UnityEngine;
using UnityEngine.UI;

public class Search : MonoBehaviour
{
    public event Action<List<Note>> Searching;
    
    [SerializeField] private GameObject inputGameObject;
    [SerializeField] private InputField inputField;


    private string tempText;
    
    private void Awake()
    {
        tempText = inputField.text;
    }

    public void OpenClose()
    {
        inputGameObject.SetActive(!inputGameObject.activeSelf);
    }
    public void OnChanged()
    {
        if(!Input.GetKeyDown(KeyCode.Escape))
        {
            tempText = inputField.text;
            Filter();
        }
    }
    public void OnEndEdit()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            inputField.text = tempText;
            Filter();
        }
    }


    private async void Filter()
    {
        var uuid = NetworkController.Instance.User.Uuid;

        var searchTask = inputField.text == "" ? 
            Database.Get(uuid+"/notes") : Database.SearchNote(uuid, inputField.text);
        
        await searchTask;


        var notes = new List<Note>();
        foreach (var item in searchTask.Result.Children)
        {
            var note = Database.Deserialize<Note>(item);
            notes.Add(note);
        }

        notes.Reverse();
        Searching?.Invoke(notes);
    }

}
