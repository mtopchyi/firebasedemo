using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Firebase.Storage;
using UnityEngine;
using static Firebase.Storage.FirebaseStorage;

public static class Storage
{
    private static FirebaseStorage storage;
    private static StorageReference storageReference;
    
    public static void InitStorage(string uuid)
    {
        storage = FirebaseStorage.DefaultInstance;
        storageReference = storage.GetReference(uuid);
    }


    public static Task<StorageMetadata> Upload(string uuid, string filePath)
    {
        return storageReference.Child(uuid).PutFileAsync(filePath);
    }
}
