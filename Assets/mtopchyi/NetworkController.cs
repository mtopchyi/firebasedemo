using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Firebase.Auth;
using Firebase.Database;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NetworkController : MonoBehaviour
{
    public event Action Ready;
    public event Action Update;
    
    public static NetworkController Instance;

    public User User => user;

    [SerializeField] private Auth auth;
    
    private User user;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        
        user = null;
        
        Authentication.InitFirebase();
    
        InitUser();
    }

    private async void InitUser()
    {
        var uuid = "";
        if(PlayerPrefs.HasKey("uuid"))
            uuid = PlayerPrefs.GetString("uuid");
        else
        {
            auth.OpenClose();
            return;
        }
        Storage.InitStorage(uuid);
        var noteTask = Database.Get(uuid);
        await noteTask;
        
        if (noteTask.IsCompleted)
        {
            Debug.Log("Complete loading");
        }
        if (noteTask.IsFaulted || noteTask.IsCanceled)
        {
            Debug.Log("something wrong...");
        }
        
        user = Database.Deserialize<User>(noteTask.Result);
        Ready?.Invoke();
    }

    public async Task<string> SignIn(string email, string password)
    {
        var authTask = Authentication.SignIn(email, password);

        await authTask;

        var uuid = authTask.Result.UserId;
        
        var userTask = Database.Get(uuid);

        await userTask;

        user = Database.Deserialize<User>(userTask.Result);
        Storage.InitStorage(uuid);
        Debug.Log("singIn");
        
        Update?.Invoke();

        return uuid;
    }
    
    public async Task<string> SignUp(string email, string password)
    {

        var userTask = Authentication.SignUp(email, password);

        await userTask;

        var uuid = userTask.Result.UserId;
        
        user = new User(uuid);
        Storage.InitStorage(uuid);
        Debug.Log("singUp");

        UpdateDatabase();

        return uuid;
    }

    public void EditNote(Note note)
    {
        user.EditNote(note);   
        UpdateDatabase();
    }

    public void AddNote(Note note)
    {
        user.AddNote(note);   
        UpdateDatabase();
    }

    public void DeleteNote(Note note)
    {
        user.DeleteNote(note);   
        UpdateDatabase();
    }
    
    public void UpdateDatabase()
    {
        Database.Save(user.ToString(), user.Uuid);
        Update?.Invoke();
    }

    public void LogOut()
    {
        
    }
    
    
}
