using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Sort : MonoBehaviour
{
    public event Action<List<Note>> SortList;
    [SerializeField] private GameObject buttonContainer; 
    
    public enum SortBy
    {
        NameAscending,
        NameDescending,
        DateAscending,
        DateDescending
    }
    
    public void OpenClose()
    {
        buttonContainer.SetActive(!buttonContainer.activeSelf);
    }


    public void SortNotes(int type)
    {
        var notes = NetworkController.Instance.User.Notes;
        var sortedNotes = type switch
        {
            0 => notes.OrderBy(n => n.Date),
            1 => notes.OrderByDescending(n => n.Date),
            2 => notes.OrderBy(n => n.Title),
            3 => notes.OrderByDescending(n => n.Title),
            _ => null
        };

        SortList?.Invoke(sortedNotes.ToList());
        OpenClose();
    }

    private List<Note> OrderByDate()
    {
        return null;
    }
}
