using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class Auth : MonoBehaviour
{
    [SerializeField] private GameObject authContainer;

    [SerializeField] private GameObject logged;
    [SerializeField] private GameObject nonLogged;
    [SerializeField] private GameObject forget;
    
    
    [SerializeField] private InputField emailInput;
    [SerializeField] private InputField passwordInput;
    
    [SerializeField] private InputField forgetEmailInput;
    [SerializeField] private InputField forgetPasswordInput;

    [SerializeField] private Text emailText;

    private string email => emailInput.text;
    private string password => passwordInput.text;

    private bool isOpen = false;

    public void Awake()
    {
        Authentication.InitFirebase();
        if (PlayerPrefs.HasKey("email"))
            emailText.text = PlayerPrefs.GetString("email");
    }

    public void OpenClose()
    {
        authContainer.transform.DOScale(isOpen ? Vector3.zero : Vector3.one, .5f);
        isOpen = !isOpen;

        var temp = PlayerPrefs.HasKey("email");
        
            nonLogged.SetActive(!temp);
            logged.SetActive(temp);
       
    }

    public void ForgotPassword()
    {
        nonLogged.SetActive(false);
        logged.SetActive(false);
        forget.SetActive(true);
    }

    public async void ResetPassword()
    {
        var passwordTask = Authentication.ResetPassword(forgetEmailInput.text);

        await passwordTask;

        if (passwordTask.IsCompleted)
        {
            forget.SetActive(false);
            nonLogged.SetActive(true);
        }
        else
        {
            return;
        }

    }


    public async void SignUp()
    {
        if(InputIsEmpty()) return;

        var authTask = NetworkController.Instance.SignUp(email, password);

        await authTask;
        
        var uuid = authTask.Result;
            
        PlayerPrefs.SetString("uuid", uuid);
        PlayerPrefs.SetString("email", email);

        emailText.text = email;
        passwordInput.text = "";
        
        PlayerPrefs.Save();
        OpenClose();
        

    }

    public async void SignIn()
    {
        if (InputIsEmpty()) return;


        var uuidTask = NetworkController.Instance.SignIn(email, password);

        await uuidTask;

        var uuid = uuidTask.Result;
        
        emailText.text = email;

        PlayerPrefs.SetString("uuid", uuid);
        PlayerPrefs.SetString("email", email);
        
        passwordInput.text = "";
        PlayerPrefs.Save();
        OpenClose();
    }
    
    public void LogOut()
    {
        PlayerPrefs.DeleteAll();
        PlayerPrefs.Save();
        
        logged.SetActive(false);
        nonLogged.SetActive(true);
    }

    private bool InputIsEmpty()
    {
        return email == "" || password == "";
    }
    
}
