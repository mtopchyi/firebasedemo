using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct UserData
{

    public List<Note> Notes;

    public UserData(List<Note> notes)
    {
        
        Notes = notes;
    }

    public void AddNewNote(Note note)
    {
        Notes.Add(note);
    }
    
    
    

}
