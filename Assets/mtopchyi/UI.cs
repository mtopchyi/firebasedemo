using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UnityEngine;

public class UI : MonoBehaviour
{
    [SerializeField] private Transform scrollViewContent;
    [SerializeField] private GameObject notePrefab;

    [SerializeField] private NoteView noteView;
    [SerializeField] private NoteView editNoteView;

    [SerializeField] private Sort sort;
    [SerializeField] private Search search;


    private void Start()
    {
        search.Searching += InitSortNotes;
        sort.SortList += InitSortNotes;
        NetworkController.Instance.Ready += InitNotes;
        NetworkController.Instance.Update += UpdateList;
    }

    public void CreateNewNote()
    {
        noteView.Open();
    }
    
    private void CloseNewNoteView()
    {
        noteView.Close();
    }

    private void Update()
    {
        
        if (Input.GetKeyDown(KeyCode.Escape) && noteView.IsOpen) CloseNoteView(true);

        if (Input.GetKeyDown(KeyCode.Escape) && editNoteView.IsOpen) CloseNoteView(false);
    }


    // ReSharper disable Unity.PerformanceAnalysis
    private void CloseNoteView(bool isNew)
    {
        if (isNew)
            noteView.Close();
        else
            editNoteView.CloseNote();
            
        PlayerPrefs.SetString("uuid", NetworkController.Instance.User.Uuid);
        PlayerPrefs.Save();

    }

    private void ClearListNotes()
    {
        
        foreach (Transform child in scrollViewContent)
        {
            Destroy(child.gameObject);
        }
    }

    private void InitNotes()
    {

        NetworkController.Instance.User.Notes.Reverse();
        
        //var tempNotes = Database.Instance.user.notes.
        foreach (var note in NetworkController.Instance.User.Notes)
        {
            var no = Instantiate(notePrefab, scrollViewContent).GetComponent<NoteObject>();
            no.Init(note, editNoteView);
        }
        
        NetworkController.Instance.User.Notes.Reverse();
    }

    private void InitSortNotes(List<Note> notes)
    {
        ClearListNotes();
        if (notes.Count == 0)
            return;
        foreach (var note in notes)
        {
            var no = Instantiate(notePrefab, scrollViewContent).GetComponent<NoteObject>();
            no.Init(note, editNoteView);
        }
    }
    

    private void UpdateList()
    {
        ClearListNotes();
        InitNotes();
    }
}
